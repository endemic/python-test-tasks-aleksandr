<?php


namespace App\Builder;


use App\Interfaces\UserInterface;

class Seller extends UserMain implements UserInterface
{

    const USER_TYPE='Seller';

    private $balance;

    private $product_count;

    public function __construct($id, $name, $earnings_balance, $product_count)
    {
        parent::__construct($id, $name);
        $this->balance = $earnings_balance;
        $this->product_count = $product_count;
    }

    public function getData(){
        return [
            'user-type'=>self::USER_TYPE,
            'id'=>parent::getId(),
            'name'=>parent::getName(),
            'earnings-balance'=>$this->balance,
            'product-count'=>$this->product_count
        ];
    }

}