<?php


namespace App\Builder;


use App\Interfaces\UserInterface;

class Administrator extends UserMain implements UserInterface
{

    const USER_TYPE='Administrator';

    private $permissions;

    public function __construct($id, $name, $permissions)
    {
        parent::__construct($id, $name);
        $this->permissions = $permissions;
    }

    public function getData(){
        return [
            'user-type'=>self::USER_TYPE,
            'id'=>parent::getId(),
            'name'=>parent::getName(),
            'permissions'=>$this->permissions,
        ];
    }

}