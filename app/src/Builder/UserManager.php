<?php


namespace App\Builder;


use App\Interfaces\UserInterface;

class UserManager
{


    public function __construct()
    {
    }

    public function getUserInfo(UserInterface $user){

        return join(PHP_EOL,array_map(function ($item) use ($user){
            return sprintf('%s : %s',$item, $user->getData()[$item]);
        },array_keys($user->getData()))).PHP_EOL.PHP_EOL;

    }

}