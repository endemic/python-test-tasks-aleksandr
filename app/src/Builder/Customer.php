<?php


namespace App\Builder;


use App\Interfaces\UserInterface;

class Customer extends UserMain implements UserInterface
{

    const USER_TYPE='Customer';

    private $balance;

    private $purchase_count;

    public function __construct($id, $name, $balance, $purchase_count)
    {
        parent::__construct($id, $name);
        $this->balance = $balance;
        $this->purchase_count = $purchase_count;
    }

    public function getData(){
        return [
            'user-type'=>self::USER_TYPE,
            'id'=>parent::getId(),
            'name'=>parent::getName(),
            'balance'=>$this->balance,
            'purchase-count'=>$this->purchase_count
        ];
    }

}