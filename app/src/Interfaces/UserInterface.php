<?php


namespace App\Interfaces;


interface UserInterface
{

    /**
     * @return array
     */
    public function getData();

}