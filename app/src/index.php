<?php

namespace App;

use App\Builder\Administrator;
use App\Builder\Customer;
use App\Builder\Seller;
use App\Builder\UserManager;
use Dotenv\Dotenv;

require_once '../vendor/autoload.php';

$dotenv = Dotenv::createImmutable(__DIR__.'/../../');
$dotenv->load();

$customer = new Customer(1, 'John', '100.55', 34);
$seller = new Seller(2, 'Alex', 550.87, 781);
$administrator = new Administrator(3, 'Arnold', '{reports, sales, users}');


$userManager = new UserManager();

echo $userManager->getUserInfo($customer);
echo $userManager->getUserInfo($seller);
echo $userManager->getUserInfo($administrator);

