1. Show Stores, that have products with Christmas, Winter Tags
```
SELECT DISTINCT(s1.name) FROM Store as s1 LEFT JOIN Product as p1 ON s1.id = p1.store_id LEFT JOIN TagConnect as tc1 ON p1.id=tc1.product_id LEFT JOIN Tag as t1 ON tc1.tag_id=t1.id WHERE t1.tag_name IN ("Winter", "Christmas")
```

2. Show Users, that never bought Product from Store with id == 5
```
SELECT * FROM User WHERE User.id NOT IN (SELECT DISTINCT(u1.id) FROM User as u1 LEFT JOIN `Order` as o1 ON u1.id=o1.customer_id LEFT JOIN OrderItem as oi1 ON o1.id=oi1.order_id LEFT JOIN Product as p1 ON oi1.product_id=p1.id LEFT JOIN `Store` as s1 ON p1.store_id=s1.id WHERE s1.id != 5)
```

3. Show Users, that had spent more than $1000 
```
SELECT u1.id, SUM(p1.price) AS summary FROM User as u1 LEFT JOIN `Order` as o1 ON u1.id=o1.customer_id LEFT JOIN OrderItem as oi1 ON o1.id=oi1.order_id LEFT JOIN Product as p1 ON oi1.product_id=p1.id GROUP BY u1.id HAVING summary > 1000
```

4. Show Stores, that have not any Sells
```
SELECT * FROM Store LEFT JOIN (SELECT s1.id, s1.name, SUM(p1.price) summary FROM `Order` as o1 LEFT JOIN OrderItem as oi1 ON o1.id=oi1.order_id LEFT JOIN Product as p1 ON oi1.product_id=p1.id LEFT JOIN `Store` as s1 ON p1.store_id=s1.id GROUP by s1.id) as custom_search ON Store.id=custom_search.id WHERE custom_search.summary is NULL
```

5. Show Mostly sold Tags
```
SELECT * FROM Tag,(SELECT tc1.tag_id, SUM(p1.price) summary FROM Store as s1 LEFT JOIN Product as p1 ON s1.id=p1.store_id LEFT JOIN OrderItem AS oi1 ON oi1.product_id=p1.id LEFT JOIN TagConnect as tc1 ON p1.id=tc1.product_id GROUP by tc1.tag_id) as custom_search WHERE Tag.id=custom_search.tag_id ORDER by custom_search.summary DESC LIMIT 0,1
```

6. Show Monthly Store Earnings Statistics 
```
SELECT custom_search.store_name, SUM(custom_search.summary), DATE_FORMAT(order_date, "%Y-%m") AS month FROM (SELECT p1.name as product_name, s1.name as store_name, s1.id as store_id, o1.order_date, SUM(p1.price) as summary FROM `Order` as o1 LEFT JOIN OrderItem as oi1 ON o1.id=oi1.order_id LEFT JOIN Product as p1 ON oi1.product_id=p1.id LEFT JOIN `Store` as s1 ON p1.store_id=s1.id GROUP by p1.id, o1.order_date) as custom_search GROUP by month, custom_search.store_id ORDER by custom_search.store_id, month
```