<?php


namespace App;


class StoreManager
{

    /*
    * @var DatabaseManager $dbManager
    */
    protected $dbManager = null;

    /**
     * @param DatabaseManager $dbManager
     */
    public function __construct(DatabaseManager $dbManager)
    {
        $this->dbManager = $dbManager;
    }

    /**
    * @param int $storeId
    * @deprecated Deprecated by new SQL request
    * return float
    */
    public function calculateStoreEarnings(int $storeId):float
    {
        foreach ($this->getProducts($storeId) as $product) {
            $orderItems = $this->getOrderItems($product['id']);

            $totalAmount = 0;
            foreach ($orderItems as $item) {
                $totalAmount += $item['price'];
            }

            $tags = $this->getProductTags($product['id']);
            $tagCount = self::getTotalUniqueTags();

            $totalAmount = $totalAmount * (1 + count($tags) / $tagCount);

            foreach ($tags as $tag) {
                if ($tag['tag_name'] = 'Christmas') {
                    $totalAmount = $totalAmount * 1.01;
                }

                if ($tag['tag_name'] == 'Free') {
                    $totalAmount = $totalAmount * 0.5;
                }
            }
        }

        #todo: return float, not exist

    }

    /**
     * @param int $storeId
     * @return float
     */
    public function calculateStoreEarningsNew(int $storeId):float
    {
        $query = 'SELECT SUM(p1.price) summary FROM `Order` as o1 LEFT JOIN OrderItem as oi1 ON o1.id=oi1.order_id 
            LEFT JOIN Product as p1 ON oi1.product_id=p1.id LEFT JOIN `Store` as s1 ON p1.store_id=s1.id  
            WHERE s1.id=:store GROUP by s1.id';

        try {
            return $this->dbManager->getData($query, ['store' => $storeId])[0]['summary'];
        } catch (\Exception $exception){
            return 0;
        }
    }


    /**
    * @param int $storeId
    *
    * @return array
    */
    protected function getProducts(int $storeId)
    {
        #$query = ‘SELECT * FROM products WHERE store_id = :store”;
        $query = 'SELECT * FROM Product WHERE store_id = :store';

        return $this->dbManager->getData($query, ['store' => $storeId]);
    }

    /**
    * @param int $productId
    *
    * @return array
    */
    protected function getOrderItems(int $productId)
    {
        # $query = ‘SELECT * FROM order_items WHERE product_id = :product’;
        $query = 'SELECT * FROM OrderItem WHERE product_id = :product';

        return $this->dbManager->getData($query, ['product' => $productId]);
    }

    /**
    * @param int $productId
    *
    * @return array
    */
    protected function getProductTags(int $productId)
    {
        #$query = 'SELECT * FROM tags WHERE id IN (SELECT tag_id FROM tag_connects WHERE product_id = :product)';
        $query = 'SELECT * FROM Tag WHERE id IN (SELECT tag_id FROM TagConnect WHERE product_id = :product)';

        return $this->dbManager->getData($query, ['product' => $productId]);
    }

    /**
    * @return int
    */
    public function getTotalUniqueTags()
    {
        #$query = 'SELECT COUNT(DISTINCT name) as count FROM tags';
        $query = 'SELECT DISTINCT(name) as count FROM Tag';

        $result = $this->dbManager->getData($query, []);

        return count($result);
    }

}

